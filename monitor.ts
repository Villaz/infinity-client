var Promise = require("bluebird")
var os  = require('os-utils')
var influx = require('influx')

function uploadPoints(){
  var client = influx({
    host : '52.24.246.168',
    port : 8086, // optional, default 8086
    protocol : 'http', // optional, default 'http'
    username : 'root',
    password : 'Espronceda',
    database : 'servers'
  })

  var hostname = require('os').hostname()

  var points_load1 = [ {hostname:hostname, value : os.loadavg(1), time : new Date()}]
  var points_load5 = [ {hostname:hostname, value : os.loadavg(5), time : new Date()}]
  var points_load15 = [{hostname:hostname,value : os.loadavg(15), time : new Date()}]

  var series = {
      load1 : points_load1,
      load5 : points_load5,
      load15 : points_load15
    };

    client.writeSeries(series)
}

setInterval(uploadPoints,15*1000)
