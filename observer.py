__author__ = 'Luis Villazon Esteban'

import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from pymongo import MongoClient
import pymongo
import ntpath
import argparse
import os

try:
    mongo_db_url = os.environ['MONGO_DB']
    mongo_collection = os.environ['MONGO_COLLECTION']
except:
    mongo_db_url = None
    mongo_collection = None

try:
    site_global = os.environ['SITE']
except:
    site_global = None

try:
    experiment = os.environ['EXPERIMENT'].upper()
except:
    experiment = None

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--id", nargs='?', help="VM identifier")
parser.add_argument("-p", "--path", nargs='?', help="File to observe")
parser.add_argument("-n", "--name", nargs='?', help="Name to use to save the file")
args = parser.parse_args()


class StartEvent(FileSystemEventHandler):

    def on_modified(self, event):
        super(StartEvent, self).on_modified(event)
        if event.src_path == args.path:
            doc = {args.name: open(args.path).read()}
            db[mongo_collection].find_one_and_update({'hostname': args.id}, {'$set': doc})

if __name__ == "__main__":

    client = MongoClient(mongo_db_url)
    db = client.infinity

    path, file = ntpath.split(args.path)
    event_handler = StartEvent()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except Exception as e:
        observer.stop()
    observer.join()
