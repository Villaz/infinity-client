var should = require('should');
var Promise = require("bluebird");
var client = require("../index.js");
var MongoClient = require('mongodb').MongoClient
var moment = require('moment')

describe('Infinity Client tests', function() {

  var url = 'mongodb://luis:Espronceda@ds062797.mongolab.com:62797/infinity_test'
  var dbG = undefined

  beforeEach(function(done) {
    this.timeout(30000);
    var servers = [{id:1,hostname:"host1",createdTime:moment().unix()}]
    MongoClient.connect(url, function(err, db) {
      dbG = db
      var collection = db.collection('servers')
      var insert = Promise.promisify(collection.insert)
      Promise.resolve(servers).each(function(server){
        collection.insert(server)
      }).then(function(){
        done()
      })
    })
  })

  afterEach(function(done){
    MongoClient.connect(url, function(err, db) {
      var collection = db.collection('servers')
      var remove = Promise.promisify(collection.remove)
      collection.remove({},function(err,col){done()})
    })
  })

  it('Boot VM',function(done){
    client.updateDB(dbG,"host1","boot").then(function(){
      var collection = dbG.collection('servers')
      collection.findOne({hostname:'host1',boot:{$exists:true}},function(err,result){
        should.exists(result)
        result.state.should.be.exactly("STARTED")
        done()
      })
    })
  })

  it('Run VM',function(done){
    client.updateDB(dbG,"host1","start").then(function(){
      var collection = dbG.collection('servers')
      collection.findOne({hostname:'host1',start:{$exists:true}},function(err,result){
        should.exists(result)
        result.state.should.be.exactly("RUNNING")
        done()
      })
    })
  })

  it('end VM',function(done){
    client.updateDB(dbG,"host1","end").then(function(){
      var collection = dbG.collection('servers')
      collection.findOne({hostname:'host1',end:{$exists:true}},function(err,result){
        should.exists(result)
        result.state.should.be.exactly("ENDED")
        done()
      })
    })
  })


  it('heartbeat VM',function(done){
    client.updateDB(dbG,"host1","heartbeat").then(function(){
      var collection = dbG.collection('servers')
      collection.findOne({hostname:'host1',heartbeat:{$exists:true}},function(err,result){
        should.exists(result)
        done()
      })
    })
  })


  it('Check location is defined when boot the VM',function(done){
    client.updateDB(dbG,"host1","boot").then(function(){
      var collection = dbG.collection('servers')
      collection.findOne({hostname:'host1',boot:{$exists:true},location:{$exists:true}},function(err,result){
        should.exists(result)
        done()
      })
    })
  })

  it('Add server to db if not exists', function(done){
    var time = moment().unix()
    client.updateDB(dbG,"host1-a-"+time,"boot").then(function(){
      var collection = dbG.collection('servers')
      collection.findOne({hostname:"host1-a-"+time},function(err,result){
        should.exists(result)
        done()
      })
    })
  })

})
