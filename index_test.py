import pymongo
import argparse
import threading
import os
import os.path
import time
import ipgetter
import requests
from geoip import geolite2
from pymongo import MongoClient
import json


mongo_db_url = os.environ['MONGO_DB']
try:
    site_global = os.environ['SITE']
except:
    site_global = None

try:
    experiment = os.environ['EXPERIMENT'].upper()
except:
    experiment = None

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--id", nargs='?', help="VM identifier")
parser.add_argument("-t", "--type", nargs='?', help="Action type")
args = parser.parse_args()


def do_every(interval, worker_func, iterations=0):
    if iterations != 1:
        threading.Timer(
            interval,
            do_every, [interval, worker_func, 0 if iterations == 0 else iterations-1]
        ).start()
    worker_func()


def send_message(doc):
    client = MongoClient(mongo_db_url)
    db = client.infinity
    capped = db.capped_servers

    id = args.id
    type = args.type.upper()

    cap = {'hostname': id,
           'state': type,
           'time': int(time.mktime(time.gmtime(time.time()))),
           'site': site_global.upper()}
    if experiment is not None:
        cap['experiment'] = experiment
    capped.insert_one(cap)
    db.computer_test.find_one_and_update({'hostname': args.id}, {'$set': doc})



def do_heartbeat():
    client = MongoClient(mongo_db_url)
    db = client.infinity
    capped = db.capped_servers

    id = args.id
    #doc = {'hostname': id, 'state':'HEARTBEAT', 'time': str(time.time()).split('.')[0], 'site': site_global.upper()}
    #capped.insert_one(doc)
    db.computer_test.find_one_and_update({'hostname': args.id},{'$set': {'heartbeat': int(time.mktime(time.gmtime(time.time())))}})


def location():
    return geolite2.lookup(ipgetter.myip())


def send_info_to_cartoDB():
    id = args.id
    type = args.type
    loc = location().location

    sql = "INSERT INTO public.servers (the_geom, description, type, name) values(ST_SetSRID(ST_Point("+str(loc[1])+","+str(loc[0])+"),4326),'"+id+"','"+type+"','"+id+"')"
    key = "344dc4313e6c105e3501ee807481e097f83fe69b"
    params = {'q': sql, 'api_key': key}
    requests.get("http://villazonpersonal.cartodb.com//api/v2/sql", params=params)
    if 'SITE' in os.environ:
        site = os.environ['SITE']
        sql = "INSERT INTO public.servers_site (the_geom, description, name) values(ST_SetSRID(ST_Point("+str(loc[1])+","+str(loc[0])+"),4326),'"+id+"','"+site+"')"
        requests.get("http://villazonpersonal.cartodb.com//api/v2/sql", params=params)


def save_s3():
    import boto3

    aws_bucket = os.environ['AWS_BUCKET']
    aws_key_id = os.environ['AWS_KEY_ID']
    aws_private_key = os.environ['AWS_ACCESS_KEY']

    host_id = args.id

    client = boto3.client('s3', aws_access_key_id=aws_key_id,
                          aws_secret_access_key=aws_private_key,
                          )
    try:
        client.put_object(ACL='public-read',
                          Body=open('/tmp/outputs.tar.gz', 'r').read(),
                          Bucket=aws_bucket,
                          Key="%s/%s/%s" % (site_global, host_id, 'outputs.tar.gz'))
    except:
        pass


if args.type == 'heartbeat':
    do_every(180, do_heartbeat)
else:
    now = int(time.mktime(time.gmtime(time.time())))
    document = None
    if args.type == 'boot':
        loc = location()
        document = {'boot': now, 'state': 'BOOTED', 'location': {'ip': loc.ip, 'location': loc.location, 'country': loc.country}}
    if args.type == 'start':
        document = {'start': now, 'state': 'STARTED'}

    if args.type == 'end':
        if os.path.isfile('/etc/machineoutputs/shutdown_message'):
            file = open('/etc/machineoutputs/shutdown_message', 'r').read()
        else:
            file = "500 Unknown"
        document = {'end': now, 'state': 'ENDED', 'reason': file}
        save_s3()
    send_message(document)
    send_info_to_cartoDB()

