var externalip = require('external-ip')();
var geoip = require("geoip-lite");
var program = require("commander");
var Promise = require("bluebird");
var moment = require("moment");
var MongoClient = require('mongodb').MongoClient;
var AWS = require('aws-sdk');
var nconf = require('nconf');
nconf.env();
AWS.config.apiVersions = {
    s3: '2006-03-01',
};
var sendGridUser = nconf.get('SEND_GRID_USER');
var sendGridPassword = nconf.get('SEND_GRID_PASSWORD');
var mongoDB = nconf.get('MONGO_DB');
var bucket = nconf.get("AWS_BUCKET");
var s3KeyID = nconf.get("AWS_KEY_ID");
var s3AccessKey = nconf.get("AWS_ACCESS_KEY");
var cartoDB = nconf.get("CARTO_DB");
var sendgrid = require("sendgrid")(sendGridUser, sendGridPassword);
program.version('0.0.1')
    .option('-i --id <identifier>', 'Machine Identifier')
    .option('-s --site <site>', 'Site where is deployed the VM')
    .option('-t --type <type>', 'Type update', /^(boot|test|start|heartbeat|end)$/i, 'heartbeat')
    .option('-l --logs <logs>', 'Directory where logs are stored, use to upload to S3')
    .parse(process.argv);
var exitMessage = undefined;
var processError = function (err) {
    var payload = {
        to: 'villazonpersonal@gmail.com',
        from: 'vcycle@cern.ch',
        subject: 'Error uploading files to S3',
        text: JSON.stringify(err)
    };
    sendgrid.send(payload, function (err, json) {
        if (err) {
            return console.error(err);
        }
    });
};
function updateDB(db, id, type, ip, logs) {
    var now = moment().unix();
    var document = {};
    switch (type) {
        case 'boot':
            document = { boot: now, state: 'BOOTED', location: ip };
            break;
        case 'start':
            document = { start: now, state: 'STARTED' };
            break;
        case 'test':
            document = { test: now, state: 'TEST' };
            break;
        case 'end':
            document = { end: now, state: 'ENDED' };
            break;
        case 'heartbeat':
            document = { heartbeat: now };
            break;
    }
    return updateDocument(id, db, type, document, bucket, logs).then(function () {
        return addToCappedCollection(db, id, document['state']);
    });
}
function updateDocument(id, db, type, document, bucket, logs) {
    if (type == 'boot' && bucket !== undefined) {
        createS3AndUploadBootFile(id)
            .catch(processError)
            .finally(function (files) {
            document['s3'] = files;
            return updateField(db, id, document);
        });
    }
    else if (type === 'test' && bucket !== undefined) {
        return saveTestFile(id, "/root/test-results.tgz").then(function (file) {
            document['testFile'] = file;
        }).catch(processError).finally(function () {
            return updateField(db, id, document);
        });
    }
    else if (type == 'end' && bucket !== undefined) {
        return saveLogFiles(id, logs).then(function (files) {
            document['s3'] = files;
        }).catch(processError)
            .finally(function () {
            var isThere = require('is-there');
            if (isThere('/etc/machineoutputs/shutdown_message'))
                document['reason'] = require("fs").readFileSync('/etc/machineoutputs/shutdown_message');
            else
                document['reason'] = 'Unknown shutdown';
            return updateField(db, id, document);
        });
    }
    else {
        return updateField(db, id, document);
    }
}
function getIP() {
    var external = Promise.promisify(externalip);
    return external().then(function (ip) {
        return geoip.lookup(ip);
    }).catch(function (err) {
        console.log(err);
        return "0.0.0.0";
    });
}
function updateField(db, id, document) {
    return new Promise(function (resolve, reject) {
        var collection = db.collection('computers');
        collection.update({ 'hostname': id }, { $set: document }, function (err, result) {
            if (err) {
                console.log(err);
                reject(err);
            }
            resolve();
        });
    });
}
function addToCappedCollection(db, id, state) {
    if (state === void 0) { state = 'HEARTBEAT'; }
    var site = id.substr(id.indexOf("-") + 1);
    site = site.substr(0, site.lastIndexOf("-"));
    var doc = { hostname: id, state: state, time: moment().unix(), site: site.toUpperCase() };
    return new Promise(function (resolve, reject) {
        var options = { capped: true, size: 10000, max: 1000, w: 1 };
        db.createCollection("capped", options, function (err, collection) {
            if (err) {
                console.log(err);
                reject(err);
            }
            else {
                collection.insert(doc, function (err, result) {
                    resolve();
                });
            }
        });
    });
}
function createS3AndUploadBootFile(id) {
    var s3 = new AWS.S3({ accessKeyId: s3KeyID,
        secretAccessKey: s3AccessKey,
        endpoint: new AWS.Endpoint('s3-us-west-2.amazonaws.com') });
    return new Promise(function (resolve, reject) {
        s3.createBucket({ Bucket: bucket + '/' + id + '/' }, function (err, data) {
            if (err)
                reject(err);
            var fs = require('fs');
            var data = fs.readFileSync('/var/log/boot.log');
            var params = { Bucket: bucket + '/' + id,
                Key: "boot.log",
                Body: data,
                ACL: 'public-read',
                ContentType: 'text/plain' };
            s3.putObject(params, function (err, data) {
                if (err)
                    reject(err);
                resolve(bucket + '/' + id + '/boot.log');
            });
        });
    });
}
function saveTestFile(id, file) {
    var s3 = new AWS.S3({ accessKeyId: s3KeyID,
        secretAccessKey: s3AccessKey,
        endpoint: new AWS.Endpoint('s3-us-west-2.amazonaws.com') });
    var fs = require("fs");
    var data = fs.readFileSync(file);
    var params = { Bucket: bucket + '/' + id,
        Key: "boot.log",
        Body: data,
        ACL: 'public-read',
        ContentType: 'text/plain' };
    return new Promise(function (resolve, reject) {
        s3.putObject(params, function (err, data) {
            if (err)
                reject(err);
            resolve(bucket + '/' + id + '/test-results.tgz');
        });
    });
}
function saveLogFiles(id, path) {
    var s3 = new AWS.S3({ accessKeyId: s3KeyID,
        secretAccessKey: s3AccessKey,
        endpoint: new AWS.Endpoint('s3-us-west-2.amazonaws.com') });
    var fs = require('fs');
    var promises = [];
    var uris = [];
    return Promise.resolve(fs.readdirSync(path)).each(function (file) {
        if (fs.lstatSync(path + file).isFile() && file.indexOf('~') < 0) {
            var data = fs.readFileSync(path + file);
            if (file === 'shutdown_message')
                exitMessage = data.toString();
            var params = { Bucket: bucket + '/' + id,
                Key: file,
                Body: data,
                ACL: 'public-read',
                ContentType: 'text/plain' };
            promises.push(new Promise(function (resolve, reject) {
                s3.putObject(params, function (err, data) {
                    if (err)
                        reject(err);
                    resolve('https://s3-us-west-2.amazonaws.com/' + bucket + '/' + id + '/' + file);
                });
            }));
        }
    }).then(function () {
        return Promise.all(promises);
    });
}
function doHeartbeat(id) {
    setInterval(function () {
        MongoClient.connect(mongoDB, function (err, db) {
            if (err) {
                console.log(err);
                return;
            }
            var document = { heartbeat: moment().unix() };
            return updateField(db, id, document).then(function () {
                return addToCappedCollection(db, id).then(function () {
                    db.close();
                    return Promise.resolve();
                });
            });
        });
    }, 60 * 1000);
}
function sendInfoToCartoDB(id, type, ip) {
    var http = require('http');
    var querystring = require('querystring');
    return new Promise(function (resolve, reject) {
        var sql = "INSERT INTO public.servers (the_geom, description, type, name) values(ST_SetSRID(ST_Point(" + ip.ll[1] + "," + ip.ll[0] + "),4326),'" + id + "','" + type + "','" + id + "')";
        var key = "344dc4313e6c105e3501ee807481e097f83fe69b";
        var text = querystring.stringify({ q: sql, api_key: key });
        var options = {
            port: 80,
            host: 'villazonpersonal.cartodb.com',
            path: '/api/v2/sql?' + text,
            method: "GET",
            rejectUnauthorized: false,
        };
        var req = http.request(options, function (res) {
            res.on('data', function (chunk) { });
            res.on('end', function (chunk) {
                resolve();
            });
        });
        req.end();
    }).timeout(10000);
}
if (!module.parent) {
    var end = function () { console.log("Finished the update"); process.exit(); };
    if (program.id == undefined) {
        console.error("The id is required");
        process.exit(-1);
    }
    else if (program.type === 'heartbeat')
        doHeartbeat(program.id);
    else {
        MongoClient.connect(mongoDB, function (err, db) {
            if (err) {
                console.log(err);
                processError(err);
            }
            return getIP().then(function (ip) {
                return updateDB(db, program.id, program.type, ip, program.logs)
                    .then(function () {
                    return sendInfoToCartoDB(program.id, program.type, ip);
                }).catch(processError)
                    .timeout(15000)
                    .finally().delay(15000).then(end);
            });
        });
        setTimeout(function () { process.exit(0); }, 30000);
    }
}
module.exports = { updateDB: updateDB };
